
import java.util.ArrayList;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductService {

    private static ArrayList<Product> productList = new ArrayList<>();

    static {
        load();
    }
    
    // Add new product (Add button)
    public static boolean addProduct(Product product) {
        productList.add(product);
        save();
        return true;
    }
    
    // Delete specified product
    public static boolean delProduct(Product product) {
        productList.remove(product);
        save();
        return true;
    }

    // Delete specified product from index
    public static boolean delProduct(int index) {
        productList.remove(index);
        save();
        return true;
    }
    
    // Clear all products
    public static boolean clearAllProducts() {
        productList.clear();
        save();
        return true;
    }

    // Get all products / Read
    public static ArrayList<Product> getAllProducts() {
        return productList;
    }
    
    // Get product from index
    public static Product getProduct(int index) {
        return productList.get(index);
    }

    // Update product
    public static boolean updateProduct(int index, Product product) {
        productList.set(index, product);
        save();
        return true;
    }
    
    // Save system
    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("Max.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(productList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName())
                    .log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }
    
    // Load system
    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;

        try {
            file = new File("Max.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            productList = (ArrayList<Product>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName())
                    .log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName())
                    .log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName())
                    .log(Level.SEVERE, null, ex);
        }

    }
}
